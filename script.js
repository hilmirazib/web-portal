const searchButton = document.querySelector('.btn-cari');
searchButton.addEventListener('click', async function() {
    const inputCari = document.querySelector('.input-data');
    const movies = await getMovies(inputCari.value);
    updateUI(movies);
});

function updateUIDetail(res) {
    const showMovies = showMovieDetails(res);
    const modalBody = document.querySelector('.modal-body');
    modalBody.innerHTML = showMovies;
}

function getMovies(keyword) {
    return fetch('https://newsapi.org/v2/everything?sortBy=popularity&apiKey=4e3cddd398b24676aca8891cd6e3769b&q=' + keyword)
        .then((res) => res.json())
        .then((res) => res.articles);
}

function updateUI(movies) {
    let card = '';
    movies.forEach((m) => (card += showCards(m)));
    const movieContainer = document.querySelector('.movie-container');
    movieContainer.innerHTML = card;
}

function showCards(m) {
    return `<div class="col-md-4 my-4">
                        <div class="card" style="width: 18rem">
                            <img src="${m.urlToImage}" class="card-img-top" alt="..." />
                            <div class="card-body">
                                <h5 class="card-title">Judul - ${m.title}</h5>
                                <h6 class="card-title">Penulis - ${m.author} - ${m.publishedAt}</h6>
                                <h6 class="card-subtitle mb-2 text-muted">${m.description}</h6>
                                <a href="${m.url}" class="btn btn-danger btnMovie" >View Details</a>
                            </div>
                        </div>
                    </div>`;
}